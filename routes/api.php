<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('users', 'Api\TestController@getUserData');
Route::post('users', 'Api\TestController@postUserData');
Route::get('users/delete/{id}', 'Api\TestController@deleteUser');
Route::get('users/edit/{id}', 'Api\TestController@editUser');
Route::post('users/update/{id}', 'Api\TestController@updateUser');
//Route::post('users', function (Request $request){
//    return App\testtable::create(['username' => $request->input(['name'])]);
//});
