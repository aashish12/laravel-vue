<?php

namespace App\Http\Controllers\Api;

use App\testtable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class TestController extends Controller
{
    //

    public function getUserData(){
        $datas = testtable::select('*')->orderBy('id', '=', 'DESC')->get();

        return response()->json(['data'=>$datas]);
    }

    public function postUserData(Request $request){
//        print_r($request->all());die();
        try{
            $result =new testtable();
            $result->username = $request->get('username');
            $result->email = $request->get('email');
            $result->created_at = time();
            $result->updated_at = time();

            $result->save();


            return response()->json(['data' => $result]);

        }catch(\PDOException $e){
            return response()->json(['message' => 'Something Went wrong !! Error: '.$e->getMessage()]);
        }

    }

    public function deleteUser($id){
        try{
            $result = DB::table('testtable')->where('id', '=', $id)->delete();
            return response()->json(['data' => $result]);
        }catch(\PDOException $e){
            return response()->json(['message' => 'Something Went wrong !! Error: '.$e->getMessage()]);
        }
    }

    public function editUser($id){
        try{
            $result = testtable::find($id);
            if(count($result) == 0){
                return response()->json(['message' => 'sorry !!! could not find any data']);
            }
            return response()->json(['data' => $result]);

        }catch(\PDOException $e){
            return response()->json(['message' => 'Something Went wrong !! Error: '.$e->getMessage()]);
        }
    }

    public function updateUser(Request $request, $id){
        try{
//            $result = DB::table('testtable')
//                ->where('id', '=', $id)
//                ->update([
//                    'username' => $request->get('username'),
//                    'email' => $request->get('email'),
//                    'updated_at' => Carbon::now()
//                ]);

            $result = testtable::find($id);

            $result->username = $request->get('username');
            $result->email = $request->get('email');
            $result->updated_at = time();

            $result->save();



            if($result){
                return response()->json(['message' => 'sorry !!! could not update any data']);
            }
            return response()->json(['data' => $result]);

        }catch(\PDOException $e){
            return response()->json(['message' => 'Something Went wrong !! Error: '.$e->getMessage()]);
        }
    }
}
